<?php
ini_set('display_errors', 1);
error_reporting(E_ALL & ~E_NOTICE);

# Определяем базовые настройки
$protocol = filter_input(INPUT_SERVER, 'HTTPS') ? 'https://' : 'http://';
$host = filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_SANITIZE_STRING);
define('BASE_URL', $protocol . $host);
define('ROOT', filter_input(INPUT_SERVER, 'DOCUMENT_ROOT', FILTER_SANITIZE_STRING));

# Автоподключение скриптов
include_once 'src/autoload.php';

App::init();

# Своя var_dump функция
function dump() {
    foreach(func_get_args() as $k => $v) {
        echo '<pre>';
        var_dump($v);
        echo '</pre>';
    }
}