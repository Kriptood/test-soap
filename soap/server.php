<?php
/**
 * Test SoapServer class
 */

include_once '../config.php';

//header("Content-Type: text/xml; charset=utf-8");
header("Content-Type: text/html; charset=utf-8");
header('Cache-Control: no-store, no-cache');
header('Expires: '.date('r'));

# отключаем кеширование WSDL-файла для тестирования
ini_set("soap.wsdl_cache_enabled", "0");

//Создаем новый SOAP-сервер
$server = new SoapServer(BASE_URL . "/soap/wsdl.php");
//Регистрируем класс обработчик
$server->setClass("SoapRouter");
//Запускаем сервер
$server->handle();
