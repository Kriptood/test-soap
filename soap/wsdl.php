<?php
include_once '../config.php';
header("Content-Type: text/xml; charset=utf-8");
echo '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . "\r\n";
?>
<wsdl:definitions
        <?php echo 'xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/"' ?>
        <?php echo 'xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"' ?>
        xmlns:s="http://www.w3.org/2001/XMLSchema"
        targetNamespace="<?= BASE_URL ?>"
        xmlns:tns="<?= BASE_URL ?>"
        name="MgrcSoap"
>
    <!-- Список параметров для каждого метода -->
    <wsdl:types>
        <s:schema elementFormDefault="qualified" targetNamespace="<?= BASE_URL ?>>">

            <s:element name="checkUSAZipRequest">
                <s:complexType>
                    <s:sequence>
                        <s:element name="token" nillable="true" type="s:string"/>
                        <s:element name="zipcode" nillable="false" type="s:string"/>
                    </s:sequence>
                </s:complexType>
            </s:element>

            <s:element name="checkUSAZipResult">
                <s:complexType>
                    <s:sequence>
                        <s:element name="error" nillable="false" type="s:boolean"/>
                        <s:element name="errorCode" nillable="true" type="s:string"/>
                        <s:element name="errorComment" nillable="true" type="s:string"/>
                        <s:element name="cityName" nillable="true" type="s:string"/>
                        <s:element name="stateName" nillable="true" type="s:string"/>
                        <s:element name="stateCode" nillable="true" type="s:string"/>
                        <s:element name="countyName" nillable="true" type="s:string"/>
                        <s:element name="latitude" nillable="true" type="s:string"/>
                        <s:element name="longitude" nillable="true" type="s:string"/>
                    </s:sequence>
                </s:complexType>
            </s:element>

        </s:schema>
    </wsdl:types>

    <!-- Привязка процедуры к сообщениям -->
    <wsdl:portType name="MgrcSoapPortType">
        <wsdl:operation name="checkUSAZip">
            <wsdl:input message="tns:checkUSAZipRequest"/>
            <wsdl:output message="tns:checkUSAZipResult"/>
        </wsdl:operation>
    </wsdl:portType>

    <!-- Формат процедур веб-сервиса -->
    <wsdl:binding name="MgrcSoapBinding" type="tns:MgrcSoapPortType">
        <soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http"/>
        <wsdl:operation name="checkUSAZip">
            <soap:operation soapAction="" style="document"/>
            <wsdl:input>
                <soap:body use="literal"/>
            </wsdl:input>
            <wsdl:output>
                <soap:body use="literal"/>
            </wsdl:output>
        </wsdl:operation>
    </wsdl:binding>

    <!-- Сообщения процедур -->
    <wsdl:message name="checkUSAZipRequest">
        <wsdl:part name="params" element="tns:checkUSAZipRequest"/>
    </wsdl:message>
    <wsdl:message name="checkUSAZipResult">
        <wsdl:part name="params" element="tns:checkUSAZipResult"/>
    </wsdl:message>

    <!-- Определение сервиса -->
    <wsdl:service name="MgrcSoap">
        <wsdl:port name="ChooseColourPort" binding="tns:MgrcSoapBinding">
            <soap:address location="<?= BASE_URL ?>/soap/server.php"/>
        </wsdl:port>
    </wsdl:service>

</wsdl:definitions>
