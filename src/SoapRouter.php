<?php

class SoapRouter {

    private $errors = [];

    public function __construct()  {}

    /**
     * Search USA zip code and return full data about it
     * @param $soapData
     * @return SoapResult
     */
    public function checkUSAZip($soapData)
    {
        $this->saveLog($soapData);

        if(!empty($soapData->zipcode)) {
            $soapData->zipcode = preg_replace('/[^0-9]/', '', $soapData->zipcode);
            if(strlen($soapData->zipcode) !== 5)
                $this->errors['002'] = 'ZIP code must be 5 digits';
        }
        else
            $this->errors['001'] = 'Invalid ZIP code';

        $result = [];
        if(!$this->isErrors()) {
            $db = App::$db->prepare('
                SELECT b.country_name AS countryName, a.city_name AS cityName, a.state_name AS stateName, 
                    a.state_code AS stateCode, a.county_name AS countyName, 
                    (a.coordinates) AS latitude, Y(a.coordinates) AS longitude  
                FROM zipcode AS a, country AS b WHERE a.country_id=b.country_id AND a.zipcode_literal=? LIMIT 1');
            $db->bindValue(1, $soapData->zipcode, PDO::PARAM_STR);
            $db->execute();
            $result = $db->fetch();
        }

        //$result = ['checkFuckUp' => '1200'];
        return $this->result( $result );
    }

    /**
     * Типовой ответ для всех методов
     * @param array $data
     * @return SoapResult
     */
    private function result(array $data = [])
    {
        $error = [
            'error' => !empty($this->errors),
            'errorCode' => implode(' | ', array_keys($this->errors)),
            'errorComment' => implode(' | ', $this->errors),
        ];

        $result = new SoapResult();
        $result->preResult($error + $data);

        return $result;
    }

    /**
     * @param $soapData
     */
    private function saveLog($soapData): void
    {
        $rawPost  = "Input at " . date('d.m.Y H:i:s') . ":\r\n";
        $rawPost .= file_get_contents('php://input');
        $rawPost .= "\r\n---\r\nmessageData:\r\n";
        ob_start();
        var_dump($soapData);
        $rawPost .= ob_get_clean();
        file_put_contents(ROOT . "/saveLog.txt", $rawPost);
        //file_put_contents(ROOT . "/saveLog.txt", $rawPost, FILE_APPEND | LOCK_EX);
    }

    private function isErrors(): bool
    {
        return !empty($this->errors);
    }
}