<?php


class App {

    public static $db;

    private static $_instance = null;

    public function __construct()
    {
        # Подключаем базу данных
        $dbConf = [
            'host' => 'localhost',
            'dbName' => 'boxberry-geo',
            'user' => 'root',
            'pass' => '',
        ];

        $dsn = 'mysql:host=localhost;dbname=' . $dbConf['dbName'] . ';charset=utf8';
        self::$db = new PDO($dsn, $dbConf['user'], $dbConf['pass']);
    }

    public static function init() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function __clone() {}
    private function __wakeup() {}
}
