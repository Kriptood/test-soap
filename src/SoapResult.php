<?php
/**
 * Object for SOAP response
 */

class SoapResult
{
    public $error;
    public $errorCode;
    public $errorComment;

    public function __construct()
    {
    }

    /**
     * Готовим ответ
     * @param array $data
     */
    public function preResult(array $data = [])
    {
        foreach ($data as $k => $v) {
            # Убираем числовые индексы ответа PDO::fetch()
            if(is_integer($k)) continue;

            $this->$k = $v;
        }
    }

}